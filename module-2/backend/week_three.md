## Week Three Recap

### Instructions
Fork this repository. Be sure to pull the latest changes to your local repo. Answer the questions to the best of your ability.

Try to answer them with limited amount of external research. These questions cover the majority of what we've learned this week.

Note: When you're done, submit a PR with a reflection in the comments about how this exercise went for you.

### Week 3 Questions

1. What is the entry at the command line to create a new rails app?
`rails new appname -T -d="postgresql" --skip-turbolinks --skip-sprint`
2. What do Models generally inherit from in rails?
`ApplicationRecord`
3. What do Controllers generally inherit from in a rails project?
`ApplicationController`
4. How would I create a route if I wanted to see a specific horse in my routes file assuming I'm sticking to standard conventions and that I didn't want other CRUD functionality?
`resources :horse, only: [:show]`
5. What rake task is useful when looking at routes, and what information does it give you?
`rails routes`, route_helper, route format, controller method
6. What is an example of a route helper? When would you use them?
`new_user_path`, it's a method that gives you the appropriate route, in a redirect
7. What's the difference between what `_url` and `_path` return when combined with a routes prefix?
- url is the whole thing, https:// and all
- uri is the / forward

8. What are strong params and why are they necessary?
They are a rails method to protect against sql injection
9. What role does `form_for` play in helping us create our forms?
It tells the form where to go
10. How does `form_for` know where to submit the user's input?
You give it an instance of the object
11. Create a form using a `form_for` helper to create a new `Horse`.
```ruby
form_for @horse do |f|
  f.label :name
  f.text_field :name
  f.submit
end
```
12. Why do we want to validate our models?
To ensure that the things we create have all the necessary details
13. What are the steps of the DNS lookup?
Browser > OS > ISP > Root Server > TLD Server > Name Server

### Review Questions
14. Within a feature test and given the following HTML, write the code necessary to target the following section and check the person's name?

  `<section id="personal-info">
    <h3><%= @person.name%></h3>
   </section>
  `

  ```ruby
  within '#personal-info' do
    expect(page).to have_content(person.name)
  end
  ```
15. How would you call the method `prance` from within the method `move` on a `Horse` instance?
I don't even know what this question is asking.
16. Given the following hash:

```ruby
furniture = {table: {height: 3, color: "red"}, purchased: true}
```
What is the different between how you would return true vs returning 3?  
> You have to access the nested hash

17. What is inheritance?
The way in which things can be set up to share behaviors
### Self Assessment:
Choose One:
* I was able to answer every question without relying on outside resources


Choose One:
* I feel confident about the content presented this week
