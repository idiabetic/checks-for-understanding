## Week Two - Module 2 Recap

Fork or re-pull this respository. Answer the questions to the best of your ability. Try to answer them with limited amount of external research. These questions cover the majority of what we've learned this week (which is a TON - YOU are a web developer!!!).

Note: When you're done, submit a PR.


### Week 2 Questions

1. At a high level, what is ActiveRecord? What does it do/allow you to do?

It is an object relational mapping, it is an abstraction layer for SQL. It allows
you to interact with SQL databases through ruby code


2. Assume you have the following model:

```ruby
class Team << ActiveRecord::Base
end
```

What are some methods you can call on `Team`? If these methods aren't defined in the class, how do you have access to them?

`.select, .order, .join` these are built into active record base and inherited

3. Assume that in your database, a team has the following attributes: "id", "name", owner_id". How would you find the name of a team with an id of 4? Assuming your class only included the code from question 2, how could you find the owner of the same team?
`Team.find(4).name`

4. Assume that you added a line to your `Team` class as follows:

```ruby
class Team << ActiveRecord::Base
  belongs_to :owner
end
```

Now how would you find the owner of the team with an id of 4?
`Owner.team.find(4)`
5. In a database that's holding students and teachers, what will be the relationship between students and teachers? Draw the schema diagram.
Many to one, many teachers one student.

          |- Student
          |- Student
Teacher --|- Student
          |- Studen
          |- Student

6. Define foreign key, primary key, and schema.
Foreign key is a reference to a record in another table.
Primary key is how a table keep tracks of records
Schema is a description of the database
7. Describe the relationship between a foreign key on one table and a primary key on another table.
The foreign key on one table is the primary key on a nother table. It's how SQL knows the tables are connected
8. What are the parts of an HTTP response?
Header
Message Body

### Optional Questions

1. Name your five favorite ActiveRecord methods (i.e. methods your models inherit from ActiveRecord) and describe what they do.
.find_by, .select, .where, .order, .group
2. Name your three favorite ActiveRecord rake tasks and describe what they do.
Drop - drops the db
Create - Creates the db
seed - seeds the db
3. What two columns does `t.timestamps null: false` create in our database?
created_at and updated_at
4. In a database that's holding schools and teachers, what will be the relationship between schools and teachers?
one to many
5. In the same database, what will you need to do to create this relationship (draw a schema diagram)?
You need to add the school_id as a foreign key to teachers
6. Give an example of when you might want to store information besides ids on a join table.
I have no idea
7. Describe and diagram the relationship between patients and doctors.
many to one
8. Describe and diagram the relationship between museums and original_paintings.
many to many
9. What could you see in your code that would make you think you might want to create a partial?
creating several of the same thing

### Self Assessment:
Choose One:
* I was able to answer every question without relying on outside resources

Choose One:
* I feel comfortable with the content presented this week
