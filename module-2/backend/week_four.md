## Week Four Recap

### Instructions
Fork or re-pull this repository. Answer the questions to the best of your ability.

Try to answer them with limited amount of external research. These questions cover the majority of what we've learned this week.

Note: When you're done, submit a PR with a reflection in the comments about how this exercise went for you.

### Week 4 Questions

1. What is a cookie?
A small file stored client side to store identifying information
2. What’s the difference between a session and a cookie?
A cookie can store session information but a session is stateful https
3. What’s a flash and when do you want to use flashes?
No idea
4. Why do people say “HTTP is stateless”?
Because there is no information stored about the client between requests.
5. What’s authentication? Explain.
It's the act of determining a user is who they say they are
6. What’s the difference between authentication and authorization?
Authentication checks if you know someone
Authorization checks if they are allowed to do something
7. What’s a before filter?
It validates a condition before hitting the controlelr method
8. How do we keep track of a user once they’ve logged in?
We store their info in the session
9. When do you want to namespace aresource? When do you want to nest a resource? What's the differences between those two approaches?
Namespace is based on the attribute of a thing (admin/user)
Resource is a sub thing (user/blue)
10. At a high level, what tools can you use to implement authorization? How would you use them?
Before actions, model attributes, helper methods
11. What's an enum, and what advantages does it offer? What data type needs to be in your database to use an enum? Where do you declare an enum?
I don't know
12. What are some strategies you can use to keep your views DRY?
use partial


### Reviews Questions
13. Given the following array of hashes, how would I print an alphabetical list of holidays?
```ruby
[
 {holiday: {name: "St Patrick's Day", supplies: ["Corned Beef and Cabbage"]}},
 {holiday: {name: "Halloween", supplies: ["Candy", "Costume"]}},
 {holiday: {name: "Hanukkah", supplies: ["Menorah"]}}
]
new = []
array.each do |thing|
  new << thing.name
end
new.sort!
```
14. How would you clean incoming data to ensure "$300" or "300.00" is stored as 300?
No idea

### Self Assessment:
Choose One:
* I was able to answer every question without relying on outside resources

Choose One:
* I feel confident about the content presented this week
