### Week 5 Questions

Re-pull from this repository. Answer the questions to the best of your ability. Try to answer them with limited amount of external research. These questions cover the majority of what we've learned this week (which is a TON!).

Note: When you're done, submit a PR.

### Week 5 Questions
1. How do we make flash messages display on a page?
`flash[:notice] = 'Message'`

2. Where is cart information/temporary information usually stored?
The session

3. What might be some reasons not to store a cart in our database? Are there any reasons why we would want to persist that information?
You wouldn't want to store carts in the db pre purchase because it just takes up spaces for something that may never be relevant.
It may be useful to persist this information for analytics, and for known users to access across devices.

4. What is the purpose of the asset pipeline?
To ease the use of css, photos, etc. within rails.

5. Why do we precompile our assets?
It's more efficient

6. What do each of the following tags do?

```ruby
<%= stylesheet_link_tag "application" %>
<%= javascript_include_tag "application" %>
<%= image_tag "rails.png" %>
```
They include the precompiled media files

7. What are some of the elements of a great read me? What are some of the benefits of taking the time to update a readme for our project?
It has user empathy, setup instructions, gem info.

8. What are the top four accessibility issues that we as developers should be aware of?
I feel like we didn't cover this.

9. `before_save` is an example of a what? Where in our Rails application would we find a `before_save`?
A call back. If we needed to validate form input

10. Given the following object, how would we create a scope for all users who are active?

```ruby
User.create(name: "Happy", active: true)
```
Also didn't cover this.

11. What is the difference between a scope and a class method?
Didn't cover this either.

### Review Questions:
12. Given the following hash:

```ruby
{cart: {"17" => 4, "204" => 52, "29" => 22}}
# 12a
thing[:cart]["48"] = 4
# 12b
thing[:cart]["29"] += 1
# 12c
thing[:cart].values.sum

```

  12a. How would you add item with id of 48 with a quantity of 4?
  12b. How would you increase the quantity of item 29?
  12c. How would you find out how many items your user is thinking about purchasing?

13. What is polymorphism? How does it relate to duck-typing? What are two ways you use this in everyday Rails applications?
I don't know.
14. How would you clean the string "(630) 854-5483" to "630.854.5483"?
```ruby
string.gsub!(' ', '.')
string.gsub!('(', '')
string.gsub!(')', '')
```

### Self Assessment:
Choose One:
* I was able to answer every question without relying on outside resources

Choose One:
* I feel confident about the content presented this week
