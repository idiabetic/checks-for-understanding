## Week One - Module 2 Recap

Fork this respository. Answer the questions to the best of your ability. Try to answer them with limited amount of external research. These questions cover the majority of what we've learned this week (which is a TON!).

Note: When you're done, submit a PR.

### Week 1 Questions

1. List the five common HTTP verbs and what the purpose is of each verb.
* GET
* POST
* DELETE
* PUT
* PATCH

2. What is Sinatra?
A web framework built on top of Rack

3. What is MVC?
Model View Controller, or the design pattern we are using to build webapps

4. Why do we follow conventions when creating our actions/path names in our Sinatra routes?
No idea, so others can interact with our code and so that sinatra will know what
we are trying to do

5. What types of variables are accessible in our view templates without explicitly passing them?
Instance variables

6. Given the following block of code, how would I pass an instance variable `count` with a value of `1` to my `index.erb` template?

  ```ruby
  get '/horses' do
    erb :index
  end
  ```

```ruby
  get '/horses' do
    @count = 1
    erb :index
   end
```

7. In the same code block, how would I pass a local variable `name` with a value of `Mr. Ed` to the view?
```ruby
  get '/horses' do
    @count = 1
    @name = 'Mr. Ed'
    erb :index
   end
```
8. What's the purpose of ERB?
It allows us to embed ruby code within HTML
9. Why do I need a development AND test database?
My guess is that it is to be able to have absolute control over the test db, also
using a smaller dataset will make tests faster
10. What is CRUD and why is it important?
Create, Read, Update, Delete. They are the ways in which we interact with a database.
11. What does HTTP stand for?
Hypter Text Transfer Protocol
12. What are the two ways to interpolate Ruby in an ERB view template? What's the difference between these two ways?
```ruby
  <% #This executes ruby code but doesn't return anything%>

  <%= #This executes ruby code and renders the return value %>
```
13. What's an ORM? What does it do?
Object Relational Model, it allows us to interact with our database by calling ruby objects
instead of writing SQL code
14. What's the most commonly used ORM in ruby (Sinatra & Rails)?
Active Record
15. Let's say we have an application with restaurants. There are seven verb + path combinations necessary to provide full CRUD functionality for our restaurant application. List each of the seven combinations, and explain what each is for.

```ruby
  def get /restaurants
  end

  def post /restaurants
  end

  def put /restaurants
  end

  def delete /restauraunts
  end

  def get /restauraunts/:id
  end

  def put /restauraunts/:id
  end

  def delete /restauraunts/:id
  end
```

16. What's a migration?
It is a set of instructions used to set up the database, add tables, columns, erc...
17. When you create a migration, does it automatically modify your database?
No it does not, you have to run the migration using `rake db:migrate`

18. How does a model relate to a database?
The model is a way of interacting with a table from the database

19. What is the difference between `#new` and `#create`?
New creates an instance of the object, create does that AND inserts it into the DB

20. Given a table named `animals`, What is the SQL query that will return all info from that table?
    `id     name        number_of_legs
    -----   ------      --------------
      1     panda       4
      2     giraffe     4
      3     whale       0
      4     bird        2
    `
```sql
  SELECT * from animals;
```
21. Using the same table, What is the SQL query that will return only the animals that has 4 legs?
```sql
  SELECT
    *
  from
    animals
  where
    number_of_legs = 4;
```

### Review Questions:
22. Given a CSV file (“films.csv”) with these headers [id, title, description], how would you load these into your database to create new instances of Film?
```ruby
require 'csv'
require '/models/film'

CSV.foreach('./data/films.csv', headers: true, header_converters: :symbol) do |film|
  Item.create(id: film[:id],
              title: film[:title],
              description: film[:description)]

```
23. Given the following hash:
```
activities = {
  hiking: {cost: $0, supplies: ['hiking shoes', 'water', 'compass']},
  karaoke: {cost: $10, supplies: ['courage', 'microphone']},
  brunch: {cost: $35, supplies: ['mimosa flutes']},
  antiquing: {cost: $200, supplies: ['list of antique stores']}
}
```
How would I add 'granola bar' to things you should have when hiking?
```ruby
  activites[:hiking][:supplies] = 'granola bar'
```
24. What are the 4 Principles of OOP? Give a one sentence explanation of each.
* Encapsulation: Objects should keep things private
* Abstraction: keep it simple stupid
* Inheritance: Children use all of the behavior their parents have
* polymorphism: Different ways to carry out a command
### Self Assessment:
Choose One: (erase the others)
* I was able to answer every question without relying on outside resources

Choose One:
* I feel confident about the content presented this week
